//
//  HtmlFunctions.m
//  Mcp
//
//  Created by Ray Price on 11/17/12.
//
//

#import "HtmlFunctions.h"
#import "TextFunctions.h"
#import "ObjectiveMecab.h"
#import "MecabNode.h"
//#import "RomajiPath.h"
//#import "libkakasi.h"

@implementation HtmlFunctions

+(NSString*)stripHtmlTagsFromHtml:(NSString*)inHtml {
	// remove remove anything between <>
	NSMutableString *newString = [NSMutableString stringWithString:@""];
	BOOL insideBrackets = NO;
	for (int i=0; i < [inHtml length]; i++) {
		// check for being inside bracket
		if ([@"<" rangeOfString:[inHtml substringWithRange:NSMakeRange(i, 1)]].location != NSNotFound) {
			// set flag
			insideBrackets = YES;
		}
		else {
			if ([@">" rangeOfString:[inHtml substringWithRange:NSMakeRange(i, 1)]].location != NSNotFound) {
				// set flag
				insideBrackets = NO;
			}
			else {
				if (!insideBrackets)
					[newString appendString:[inHtml substringWithRange:NSMakeRange(i, 1)]];
			}
		}
	}
	
	return newString;
}

+(NSString*)stripRubyTagsFromHtml:(NSString*)inHtml {
    // do until no more found
    NSString *outHtml = [NSString stringWithString:inHtml];
    while ([outHtml rangeOfString:@"<rt>"].location != NSNotFound) {
        // find range
        int startPos = (int)[outHtml rangeOfString:@"<rt>"].location;
        int endPos = (int)[outHtml rangeOfString:@"</rt>"].location + 3;
        NSString *removeString = [outHtml substringWithRange:NSMakeRange(startPos, endPos-startPos+1)];
        
        // strip out
        outHtml = [outHtml stringByReplacingOccurrencesOfString:removeString withString:@""];
    }
    
    return outHtml;
}

+(NSString*)getRubyForKanji:(NSString*)inKanji andKana:(NSString*)inKana {
    // create ruby
    NSString *rubyString;
    if ([inKanji isEqualToString:inKana])
        rubyString = inKana;
    else {
        // get components
        NSArray *components = [TextFunctions getFuriganaComponentsFromKanji:inKanji andKana:inKana];
        
        // component parts
        NSString *kanaPrefix = [components objectAtIndex:0];
        NSString *kanjiPrefix = [components objectAtIndex:1];
        NSString *furigana = [components objectAtIndex:2];
        NSString *kanaSuffix = [components objectAtIndex:3];
        
        /*
        // create ruby only for kanji part
        for (int i=0; i < MIN(inKanji.length, inKana.length); i++) {
            // keep going until we hit a kanji
            NSString *thisChar = [inKanji substringWithRange:NSMakeRange(inKanji.length-i-1, 1)];
            if ([TextFunctions isKana:thisChar]) {
                kanaSuffix = [NSString stringWithFormat:@"%@%@", thisChar, kanaSuffix];
            }
            else {
                // furigana and kanji are the characters up to this point
                kanjiPrefix = [inKanji substringWithRange:NSMakeRange(0, inKanji.length-i)];
                furigana = [inKana substringWithRange:NSMakeRange(0, inKana.length-i)];
                break;
            }
        }
        
        // now word forwards incase we have any 'go' or 'o' in front
        for (int i=0; i < kanjiPrefix.length; i++) {
            // keep going until we hit a kanji
            NSString *thisChar = [kanjiPrefix substringWithRange:NSMakeRange(i, 1)];
            if ([TextFunctions isKana:thisChar]) {
                kanaPrefix = [NSString stringWithFormat:@"%@%@", kanaPrefix, thisChar];
            }
            else {
                // remove the kana from the kanji part and the furigana
                kanjiPrefix = [kanjiPrefix substringWithRange:NSMakeRange(i, kanjiPrefix.length-i)];
                furigana = [furigana substringWithRange:NSMakeRange(i, furigana.length-i)];
                break;
            }
        }
        */
        
        rubyString = [NSString stringWithFormat:@"%@<ruby>%@<rt>%@</rt></ruby>%@", kanaPrefix, kanjiPrefix, furigana, kanaSuffix];
    }
    
    return rubyString;
}

+(void)forceDrawForWebView:(UIWebView*)inWebView {
    NSArray *views = inWebView.scrollView.subviews;
    for (int i=0; i < views.count; i++) {
        UIView *view = views[i];
        if ([NSStringFromClass([view class]) isEqualToString:@"UIWebBrowserView"]) {
            [view setNeedsDisplayInRect:inWebView.bounds];
            [view setNeedsLayout];
        }
    }
}

+(NSString*)convertHtml:(NSString*)inHtml toHtmlAs:(int)inHtmlAs { // 0=kanji, 1=furi, 2=kana, 3=romaji
    
    //    return [self convertHtml2:inHtml toHtmlAs:inHtmlAs];
    
    // use mecab to translate kanji segment into furigana with ruby
    NSString *justText = [HtmlFunctions stripHtmlTagsFromHtml:inHtml];
    NSArray *returnNodes = [[ObjectiveMecab getSingleton] parseToNodeWithString:justText];
    NSMutableArray *myNodes = [NSMutableArray arrayWithArray:returnNodes];
    
    // run nodes through exceptions parser
    for (int i=0; i < myNodes.count; i++) {
        // check exceptions at this point
        for (int e=0; e < [ObjectiveMecab getSingleton].mecabExceptions.count; e++) {
            // make sure we still have room
            NSArray *thisException = [[ObjectiveMecab getSingleton].mecabExceptions objectAtIndex:e];
            int exNodeCount = (int)thisException.count / 2;
            if (exNodeCount > 0 && i+exNodeCount < myNodes.count) {
                // see if all exception nodes are found
                BOOL found = YES;
                for (int n=0; n < exNodeCount; n++) {
                    MecabNode *searchNode = [myNodes objectAtIndex:i+n];
                    if (![searchNode.surface isEqualToString:[thisException objectAtIndex:n]]) {
                        found = NO;
                        break;
                    }
                }
                // if exception found, replace with katakana
                if (found) {
                    for (int n=0; n < exNodeCount; n++) {
                        MecabNode *searchNode = [myNodes objectAtIndex:i+n];
                        [searchNode setPronunciation:[TextFunctions hiraganaToKatakana:[thisException objectAtIndex:exNodeCount+n]]];
                    }
                }
            }
        }
    }
    
    // remove any nil nodes
    int pos=0;
    while (pos < myNodes.count) {
        MecabNode *node = [myNodes objectAtIndex:pos];
        if ([node pronunciation] == nil)
            [myNodes removeObjectAtIndex:pos];
        else
            pos++;
    }
    
    // for each node, remove kana that also appears in surface (ie, strip the 'ku' from 'iku')
    for (int i=0; i < myNodes.count; i++) {
        // count kana at end of surface and remove from pronunciation
        MecabNode *node = [myNodes objectAtIndex:i];
        int kanaCount = 0;
        for (int i=(int)node.surface.length-1; i >= 0; i--) {
            NSString *surfaceChar = [node.surface substringWithRange:NSMakeRange(i, 1)];
            if (![TextFunctions containsKanji:surfaceChar])
                kanaCount++;
            else
                break;
        }
        
        // remove from pronunciation if it's not the whole thing!
        if (kanaCount > 0 && kanaCount < node.surface.length) {
            [node setPronunciation:[[node pronunciation] substringToIndex:[node pronunciation].length-kanaCount]];
            [node setSurface:[[node surface] substringToIndex:[node surface].length-kanaCount]];
        }
    }
    
    // add string and we'll use search and replace to add ruby or change to kana
    NSString *newHtmlString = [[inHtml copy] autorelease];
    
    // show as kanji, kana?
    int lastFoundLocation = 0;
    switch (inHtmlAs) {
        case 0: // kanji
        case 1: // furigana
        case 2: // kana
            // process each node
            for (int i=0; i < myNodes.count; i++) {
                // if we have pronunciation
                MecabNode *node = [myNodes objectAtIndex:i];
                if ([node pronunciation] != nil && ![node.pronunciation isEqualToString:@"！"]
                    && ![node.pronunciation isEqualToString:@"？"]) {
                    // get kanji and kana
                    NSString *kanji = node.surface;
                    // use reading not pronunciation because kinou comes out as kino- otherwise
                    // and we then can't distinguish between pa-ti- and kino-
                    NSString *kana = [node reading];
                    // but trim to length of pronuncation because 行かなくて comes to ika instead of i
                    kana = [node.reading substringWithRange:NSMakeRange(0, node.pronunciation.length)];
//                    NSString *kana = [node pronunciation];
                    NSString *furi = [TextFunctions getFuriganaFromKanji:kanji andKana:kana];
                    if (furi != nil) {
                        // replace with either furi or kana
                        NSRange found = [newHtmlString rangeOfString:kanji options:0 range:NSMakeRange(lastFoundLocation, newHtmlString.length-lastFoundLocation)];
                        if (found.location != NSNotFound) {
                            NSString *replaceString;
                            if (inHtmlAs == 1)
                                replaceString = [NSString stringWithFormat:@"<ruby>%@<rt>%@</rt></ruby>", kanji, furi];
                            else
                                if (inHtmlAs == 0)
                                    replaceString = [NSString stringWithFormat:@"%@ ",kanji];
                                else
                                    replaceString = [NSString stringWithFormat:@"%@ ",furi];
                            newHtmlString = [newHtmlString stringByReplacingCharactersInRange:found withString:replaceString];
                            lastFoundLocation = (int)found.location + (int)replaceString.length;
                        }
                    }
                }
            }
            break;
        case 3: // romaji
            
        default:
            break;
    }
    
    return newHtmlString;
}


+(NSString*)convertHtml2:(NSString*)inHtml toHtmlAs:(int)inHtmlAs { // 0=kanji, 1=furi, 2=kana, 3=romaji
    // use mecab to translate kanji segment into furigana with ruby
    NSString *justText = [HtmlFunctions stripHtmlTagsFromHtml:inHtml];;
    NSArray *returnNodes = [[ObjectiveMecab getSingleton] parseToNodeWithString:justText];
    NSMutableArray *myNodes = [NSMutableArray arrayWithArray:returnNodes];
    
    // run nodes through exceptions parser
    for (int i=0; i < myNodes.count; i++) {
        // check exceptions at this point
        for (int e=0; e < [ObjectiveMecab getSingleton].mecabExceptions.count; e++) {
            // make sure we still have room
            NSArray *thisException = [[ObjectiveMecab getSingleton].mecabExceptions objectAtIndex:e];
            int exNodeCount = (int)thisException.count / 2;
            if (exNodeCount > 0 && i+exNodeCount < myNodes.count) {
                // see if all exception nodes are found
                BOOL found = YES;
                for (int n=0; n < exNodeCount; n++) {
                    MecabNode *searchNode = [myNodes objectAtIndex:i+n];
                    if (![searchNode.surface isEqualToString:[thisException objectAtIndex:n]]) {
                        found = NO;
                        break;
                    }
                }
                // if exception found, replace with katakana
                if (found) {
                    for (int n=0; n < exNodeCount; n++) {
                        MecabNode *searchNode = [myNodes objectAtIndex:i+n];
                        [searchNode setPronunciation:[TextFunctions hiraganaToKatakana:[thisException objectAtIndex:exNodeCount+n]]];
                    }
                }
            }
        }
    }
    
    // for each node, remove kana that also appears in surface (ie, strip the 'ku' from 'iku')
    for (int i=0; i < myNodes.count; i++) {
        // if we have a pronunciation (ie, it's japanese)
        MecabNode *node = [myNodes objectAtIndex:i];
        if ([node pronunciation] != nil) {
            // count kana at end of surface and remove from pronunciation
            int kanaCount = 0;
            for (int i=0; i < node.surface.length; i++) {
                NSString *surfaceChar = [node.surface substringWithRange:NSMakeRange(node.surface.length-1-i, 1)];
                NSString *pronunChar = [TextFunctions katakanaToHiragana:[[node pronunciation] substringWithRange:NSMakeRange([node pronunciation].length-1-i, 1)]];
                if ([surfaceChar isEqualToString:pronunChar])
                    kanaCount++;
                else
                    break;
            }
            
            // remove from pronunciation if it's not the whole thing!
            if (kanaCount > 0 && kanaCount < node.surface.length) {
                // remove from pronunciation
                [node setPronunciation:[[node pronunciation] substringToIndex:[node pronunciation].length-kanaCount]];
            }
        }
    }
    
    // add string and we'll use search and replace to add ruby or change to kana
    NSMutableString *newHtmlString = [NSMutableString stringWithString:@""];
    
    // loop through nodes and rebuild html
    for (int i=0; i < myNodes.count; i++) {
        // replace with surface by default
        MecabNode *node = [myNodes objectAtIndex:i];
        NSString *replaceValue = node.surface;
        NSString *nodePos = (NSString*)[node.features objectAtIndex:0];
        
        // if there is a pronunciation
        if ([node pronunciation] != nil) {
            // get kanji and kana
            NSString *kanji = node.surface;
            NSString *kana = [node pronunciation];
            NSString *furi = [TextFunctions getFuriganaFromKanji:kanji andKana:kana];
            if (furi != nil) {
                // seperate non kanji characters into seperate string so that the
                // furigana is centered over the kanji alone
                NSString *extra = @"";
                for (int i=(int)node.surface.length-1; i >= 0; i--) {
                    NSString *thisChar = [node.surface substringWithRange:NSMakeRange(i, 1)];
                    if ([TextFunctions containsKanji:thisChar])
                        break;
                    else {
                        extra = [NSString stringWithFormat:@"%@%@", thisChar, extra];
                        kanji = [kanji substringToIndex:i];
                    }
                }
                
                // replace with either furi or kana
                if (inHtmlAs == 1) {
                    // replace with ruby furigana
                    replaceValue = [NSString stringWithFormat:@"<ruby>%@<rt>%@</rt></ruby>%@", kanji, furi, extra];
                }
                else {
                    if (inHtmlAs == 2)
                        // replace kanji with kana
                        replaceValue = [NSString stringWithFormat:@"%@%@ ",furi, extra];
                }
            }
        }
        
        // if this is a noun, adjective, adverb or verb, link to dictionary entry here
        if ([@"形容詞,動詞,副詞,名詞" rangeOfString:nodePos].location != NSNotFound) {
            NSString *dictForm = (NSString*)[node.features objectAtIndex:6];
            dictForm = [dictForm stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            replaceValue = [NSString stringWithFormat:@"<a href=""file://%@"">%@</a>", dictForm, replaceValue];
        }
        
        // replace node
        [newHtmlString appendString:replaceValue];
    }
    
    return newHtmlString;
}

@end
