//
//  HtmlFunctions.h
//  Mcp
//
//  Created by Ray Price on 11/17/12.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface HtmlFunctions : NSObject

+(NSString*)stripHtmlTagsFromHtml:(NSString*)inHtml;
+(NSString*)stripRubyTagsFromHtml:(NSString*)inHtml;
+(NSString*)convertHtml:(NSString*)inHtml toHtmlAs:(int)inHtmlAs; // 0=kanji, 1=furi, 2=kana, 3=romaji
+(NSString*)convertHtml2:(NSString*)inHtml toHtmlAs:(int)inHtmlAs;
+(NSString*)getRubyForKanji:(NSString*)inKanji andKana:(NSString*)inKana;
+(void)forceDrawForWebView:(UIWebView*)inWebView;

@end
